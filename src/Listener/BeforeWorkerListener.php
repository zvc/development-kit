<?php

declare(strict_types=1);

namespace Zct\Utils\Listener;

use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BeforeWorkerStart;
use Zct\Utils\Data\ServiceEventListenerData;

final class BeforeWorkerListener implements ListenerInterface
{
    /**
     * @return string[]
     */
    public function listen(): array
    {
        return [
            BeforeWorkerStart::class
        ];
    }

    /**
     * @param BeforeWorkerStart $event
     * @return void
     */
    public function process(object $event): void
    {
        ServiceEventListenerData::listenerStart($event->workerId);
    }
}