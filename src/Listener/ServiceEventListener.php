<?php

declare(strict_types=1);

namespace Zct\Utils\Listener;

use Hyperf\Redis\Redis;
use Hyperf\Event\ListenerData;
use ReflectionClass;
use Throwable;
use Zct\Utils\Constants\EventConstants;
use Zct\Utils\Log;
use Zct\Utils\UtilRedis;

use function Hyperf\Coroutine\go;

final class ServiceEventListener
{
    /**
     * @var ListenerData
     */
    private ListenerData $listenerData;

    /**
     * @var \Hyperf\Redis\Redis|null
     */
    private ?Redis $redis;

    /**
     * 事件类的构造函数包含的所有参数名
     * @var array
     */
    private array $constructorParameterNames = [];

    public function __construct(ListenerData $listenerData)
    {
        $this->redis        = UtilRedis::get();
        $this->listenerData = $listenerData;
        $ref                = new ReflectionClass($listenerData->event);
        if ($ref->getConstructor()) {
            $parameters = $ref->getConstructor()->getParameters();
            foreach ($parameters as $parameter) {
                $this->constructorParameterNames[] = $parameter->name;
            }
        }
        if ($this->redis) {
            go(function () use ($listenerData) {
                $this->on($listenerData->event);
            });
        }
    }

    private function on(string $event): void
    {
        try {
            if ($this->redis) {
                $this->redis->subscribe([EventConstants::SERVICE_EVENT_PR . $event], [$this, 'subscribe']);
            }
        } catch (Throwable $throwable) {
            Log::error('ServiceEvent on:' . $throwable->getMessage());
            $this->on($event);
        }
    }

    public function subscribe($redis, $chan, $msg): void
    {
        try {
            $data = json_decode($msg, true, 512, JSON_THROW_ON_ERROR);
            if (!$data) {
                $data = [];
            }
            $listener              = $this->listenerData->listener;
            $event                 = $this->listenerData->event;
            $constructorParameters = [];
            foreach ($this->constructorParameterNames as $parameterName) {
                $constructorParameters[] = $data[$parameterName] ?? null;
            }
            $listener(new $event(...$constructorParameters));
        } catch (Throwable $throwable) {
            Log::error('ServiceEvent subscribe:' . $throwable->getMessage());
        }
    }
}