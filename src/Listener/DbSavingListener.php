<?php

declare(strict_types=1);

namespace Zct\Utils\Listener;

use Hyperf\Database\Model\Events\Saving;
use Hyperf\Event\Contract\ListenerInterface;
use Throwable;
use Zct\Utils\Data\ModelJsonFieldData;

final class DbSavingListener implements ListenerInterface
{
    public function listen(): array
    {
        return [
            Saving::class
        ];
    }

    /**
     * @param Saving $event
     */
    public function process(object $event): void
    {
        try {
            $model  = $event->getModel();
            $fields = ModelJsonFieldData::getFields($model);
            foreach ($fields as $field) {
                if (isset($model->{$field}) && is_array($model->{$field})) {
                    $model->{$field} = json_encode($model->{$field}, JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR);
                }
            }
        } catch (Throwable) {
        }
    }

}