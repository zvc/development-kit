<?php

declare(strict_types=1);

namespace Zct\Utils\Listener;

use Hyperf\Database\Model\Events\Retrieved;
use Hyperf\Database\Model\Events\Saved;
use Hyperf\Event\Contract\ListenerInterface;
use Throwable;
use Zct\Utils\Data\ModelJsonFieldData;

final class DbRetrievedListener implements ListenerInterface
{
    /**
     * @return string[]
     */
    public function listen(): array
    {
        return [
            Retrieved::class,
            Saved::class
        ];
    }

    /**
     * @param object $event
     * @return void
     */
    public function process(object $event): void
    {
        try {
            $model  = $event->getModel();
            $fields = ModelJsonFieldData::getFields($model);
            foreach ($fields as $field) {
                if ($model->{$field}) {
                    $model->{$field} = json_decode($model->{$field}, true, 512, JSON_THROW_ON_ERROR);
                }
            }
        } catch (Throwable) {
        }
    }

}