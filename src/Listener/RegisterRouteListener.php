<?php

declare(strict_types=1);

namespace Zct\Utils\Listener;

use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BeforeWorkerStart;
use Hyperf\HttpServer\Router\Router;
use Hyperf\ServiceGovernance\ServiceManager;
use Zct\Utils\Constants\Grpc;
use Zct\Utils\Grpc\Container\Container;

use function Hyperf\Config\config;

class RegisterRouteListener implements ListenerInterface
{
    private ServiceManager $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @return string[]
     */
    public function listen(): array
    {
        return [
            BeforeWorkerStart::class
        ];
    }

    /**
     * @param BeforeWorkerStart $event
     * @return void
     */
    public function process(object $event): void
    {
        $servers = config('server.servers');
        if (!$servers) {
            return;
        }
        $hasGrpc = false;
        foreach ($servers as $server) {
            if ($server['name'] === Grpc::SERVER_NAME) {
                $hasGrpc = true;
                break;
            }
        }
        if (!$hasGrpc) {
            return;
        }

        $grpcServers = Container::getAll();
        if (!count($grpcServers)) {
            return;
        }

        Router::addServer(Grpc::SERVER_NAME, function () use ($grpcServers) {
            $routerData = Router::getData();
            if (!isset($routerData[0]['POST']) || count($routerData[0]["POST"]) < 1) {
                return;
            }

            foreach ($grpcServers as $server) {
                foreach ($server->methods as $method) {
                    $this->serviceManager->register($server->package . '.' . $server->name, $method->path, [
                        'server'    => Grpc::SERVER_NAME,
                        'protocol'  => 'grpc',
                        'publishTo' => $server->publishTo,
                    ]);
                }
            }
        });
    }
}