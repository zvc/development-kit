<?php

declare(strict_types=1);

namespace Zct\Utils\Listener;

use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BeforeServerStart;
use Hyperf\Framework\Event\BeforeWorkerStart;
use Throwable;
use Zct\Utils\Data\RegisteredServiceData;

final class BeforeServerListener implements ListenerInterface
{
    /**
     * @return string[]
     */
    public function listen(): array
    {
        return [
            BeforeServerStart::class
        ];
    }

    /**
     * @param BeforeWorkerStart $event
     * @return void
     */
    public function process(object $event): void
    {
        try {
            if (!file_exists(RegisteredServiceData::REGISTERED_SERVICE_FILE_PATH)){
                return;
            }
            unlink(RegisteredServiceData::REGISTERED_SERVICE_FILE_PATH);
        } catch (Throwable) {
        }
    }
}