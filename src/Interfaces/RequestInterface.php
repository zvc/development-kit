<?php

declare(strict_types=1);

namespace Zct\Utils\Interfaces;

interface RequestInterface
{
    public static function getInstance(): self;

    public function set(string $key, int|string|array $data): self;

    public function get(string $key): int|string|array|null;

    public function from(int $id, array $info = []): self;

    public function setUser(array $user): self;

    public function pack(): array;

    public static function unpack(string|array $packData): self;

    public function getFromId(): int;

    public function getFromInfo(): array;

    public function getHeader(string $key): int|string|array|null;

    public function getHeaders(): array;

    public function getTimestamp(): int;

    public function getType(): string;

    public function getRequestType(): string;

    public function getPath(): string;

    public function getParams(): array;

    public function getServerParams(): array;

    public function getServerParam(string $key): int|string|array|null;
}