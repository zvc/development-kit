<?php

declare(strict_types=1);

namespace Zct\Utils\Interfaces;

interface ResponseInterface
{
    public function set(string $msg = '', array $data = [], int $code = 0): self;

    public function success(string $msg = '', array $data = [], int $code = 1): self;

    public function error(string $msg = '', array $data = [], int $code = 0): self;

    public function json(): string;

    public static function unpack(string|array $packData): self;
}