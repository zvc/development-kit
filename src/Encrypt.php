<?php

declare(strict_types=1);

namespace Zct\Utils;

class Encrypt
{
    private static string $defaultKey = 'e682c3e81O95ddbe';

    private static string $method = 'AES-128-ECB';

    private static int $options = 0;

    /**
     * 加密
     * @param string $data
     * @param string|null $key
     * @return false|string
     */
    public static function encode(string $data, string $key = null): false|string
    {
        return openssl_encrypt($data, self::$method, $key ?? self::$defaultKey, self::$options);
    }

    /**
     * 解密
     * @param string $encrypt
     * @param string|null $key
     * @return false|string
     */
    public static function decode(string $encrypt, string $key = null): false|string
    {
        return openssl_decrypt($encrypt, self::$method, $key ?? self::$defaultKey, self::$options);
    }
}