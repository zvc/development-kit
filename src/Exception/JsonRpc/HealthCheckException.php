<?php

declare(strict_types=1);

namespace Zct\Utils\Exception\JsonRpc;

use Exception;

class HealthCheckException extends Exception
{
}