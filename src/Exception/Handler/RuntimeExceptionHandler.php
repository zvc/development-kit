<?php

declare(strict_types=1);

namespace Zct\Utils\Exception\Handler;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\Framework\Event\BeforeMainServerStart;
use Hyperf\Framework\Event\BeforeServerStart;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zct\Utils\Amqp\Amqp;
use Zct\Utils\Amqp\Producer\ExceptionProducer;

class RuntimeExceptionHandler extends ExceptionHandler
{
    private FormatterInterface $formatter;

    public function __construct(FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    public function handle(Throwable $throwable, ResponseInterface $response): ResponseInterface
    {
        try {
            $format = $this->formatter->format($throwable);
            $this->exceptionHandler(
                $throwable->getCode(),
                $throwable->getMessage(),
                $throwable->getFile(),
                $throwable->getLine(),
                $format
            );
            logger()->warning($format);
        } catch (Throwable $throwable) {
            echo $throwable->getMessage() . PHP_EOL;
        }
        return $response;
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }

    public function listen(): array
    {
        return [
            BeforeMainServerStart::class,
            BeforeServerStart::class,
        ];
    }

    public function exceptionHandler($code, $message, $file = '', $line = 0, $format = ''): bool
    {
        if (!(error_reporting())) {
            return false;
        }
        try {
            Amqp::push(ExceptionProducer::class, [
                'exceptionInfo' => [
                    'code'    => $code,
                    'message' => $message,
                    'file'    => $file,
                    'line'    => $line,
                    'format'  => $format,
                ],
                'trigger_time'  => date('Y-m-d H:i:s'),
                'requestInfo'   => internalRequest()->pack()
            ]);
        } catch (Throwable $throwable) {
            logger()->debug($throwable->getMessage());
        }
        return false;
    }
}
