<?php

declare(strict_types=1);

namespace Zct\Utils\Constants;

class Grpc
{
    public const HEALTHCHECK_SERVICE = "grpc.health.v1.Health";

    public const  HEALTHCHECK_ROUTER = '/' . self::HEALTHCHECK_SERVICE . '/Check';

    public const SERVER_NAME = 'grpc';
}