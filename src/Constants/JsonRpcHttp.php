<?php

declare(strict_types=1);

namespace Zct\Utils\Constants;

class JsonRpcHttp
{
    public const SERVER_NAME = 'jsonrpc-http';
}