<?php

declare(strict_types=1);

namespace Zct\Utils\Constants;

class JsonRpcRequestParamMode
{
    public const INDEXED     = 'indexed';
    public const ASSOCIATIVE = 'associative';
}