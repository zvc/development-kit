<?php

declare(strict_types=1);

namespace Zct\Utils\Rpc;

use Psr\Container\ContainerInterface;
use Zct\Utils\Data\ServiceClientParamData;

class ServiceClient extends \Hyperf\RpcClient\ServiceClient
{
    public function __construct(
        ContainerInterface $container,
        string $serviceName,
        string $protocol = 'jsonrpc-http',
        array $options = []
    ) {
        parent::__construct($container, $serviceName, $protocol, $options);
    }

    protected function __request(string $method, array $params, ?string $id = null)
    {
        if (count($params)) {
            $formalParams = ServiceClientParamData::getFormalParams($this->serviceInterface, $method);
            if ($formalParams && count($formalParams)) {
                $newParams = [];
                foreach ($params as $k => $v) {
                    $newParams[$formalParams[$k]] = $v;
                }
                $params = $newParams;
            }
        }
        return parent::__request($method, $params, $id);
    }

    public function __call(string $method, array $params)
    {
        return $this->__request($method, $params);
    }

    protected function setOptions(array $options): void
    {
        parent::setOptions($options);
    }
}
