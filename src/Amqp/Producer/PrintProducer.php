<?php

declare(strict_types=1);

namespace Zct\Utils\Amqp\Producer;

use Hyperf\Amqp\Message\ProducerMessage;
use function Hyperf\Support\env;

class PrintProducer extends ProducerMessage
{
    protected string       $exchange   = 'zct';
    protected array|string $routingKey = 'print';

    public function __construct($data)
    {
        $this->payload = [
            'data'         => $data,
            'service_name' => env('APP_NAME'),
        ];
    }
}
