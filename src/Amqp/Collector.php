<?php

declare(strict_types=1);

namespace Zct\Utils\Amqp;

use Hyperf\Amqp\Producer;
use Hyperf\Context\ApplicationContext;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;
use function Hyperf\Coroutine\go;
use function Hyperf\Support\env;

/**
 * 数据收集器
 */
final class Collector
{
    private float  $interval;//收集间隔
    private array  $dataContainer = [];//收集的数据
    private string $producerClassName;

    public function __construct(string $producerClassName)
    {
        $this->producerClassName = $producerClassName;
        $this->interval          = (float)env('AMQP_COLLECTOR_INTERVAL', 0.3);
    }

    /**
     * 添加一条数据
     * @param array $data
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function add(array $data): void
    {
        if ($this->interval > 0) {
            $this->addDefer($data);
        } else {
            $this->addRealTime($data);
        }
    }

    /**
     * 有延迟的添加
     * @param array $data
     */
    private function addDefer(array $data)
    {
        go(function () {
            try {
                usleep((int)($this->interval * 1000000));
                if (count($this->dataContainer)) {
                    $this->produce();
                }
            } catch (Throwable $throwable) {
                logger()->debug($throwable->getMessage());
            }
        });
        $this->dataContainer[] = $data;
    }

    /**
     * 实时添加
     * @param array $data
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function addRealTime(array $data)
    {
        $this->dataContainer[] = $data;
        $this->produce();
    }

    /**
     * 投递数据到amqp
     * @return mixed
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function produce()
    {
        $producerClassName   = $this->producerClassName;
        $dataContainer       = $this->dataContainer;
        $this->dataContainer = [];
        return ApplicationContext::getContainer()->get(Producer::class)->produce(new $producerClassName($dataContainer));
    }
}