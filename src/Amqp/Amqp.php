<?php

declare(strict_types=1);

namespace Zct\Utils\Amqp;

use Hyperf\Context\ApplicationContext;
use Throwable;
use function Hyperf\Config\config;

class Amqp
{
    private static array $container = [];

    private static ?Amqp $instance = null;

    private bool $isEnabled;

    public function __construct()
    {
        try {
            $this->isEnabled = (
                ApplicationContext::getContainer()->has('Hyperf\Amqp\ConfigProvider') &&
                config('amqp.default.host') &&
                config('amqp.default.port')
            );
        } catch (Throwable $throwable) {
            logger()->debug($throwable->getMessage());
        }
    }

    private static function instance(): self
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 判断amqp是否可用
     * @return bool
     */
    public static function isEnabled(): bool
    {
        return self::instance()->isEnabled;
    }

    public static function push(string $producerClassName, array $data): void
    {
        if (!self::instance()->isEnabled) {
            return;
        }
        if (!isset(self::$container[$producerClassName])) {
            self::$container[$producerClassName] = new Collector($producerClassName);
        }
        $data['time'] = date('Y-m-d H:i:s');
        self::$container[$producerClassName]->add($data);
    }
}