<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use JsonException;
use Hyperf\Consul\AgentInterface;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\ServiceGovernanceConsul\ConsulAgent;
use Psr\Container\ContainerInterface;
use Zct\Utils\Constants\Grpc;
use Zct\Utils\Constants\JsonRpcHttp;
use Zct\Utils\Data\RegisteredServiceData;

class ConsulDriverAspect
{
    public array $classes = [
        'Hyperf\ServiceGovernanceConsul\ConsulDriver::isRegistered',
        'Hyperf\ServiceGovernanceConsul\ConsulDriver::register',
    ];

    protected ContainerInterface $container;

    protected ConfigInterface $config;

    protected StdoutLoggerInterface $logger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger    = $container->get(StdoutLoggerInterface::class);
        $this->config    = $container->get(ConfigInterface::class);
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        switch ($proceedingJoinPoint->methodName) {
            case 'register':
                [$name, $host, $port, $metadata] = $proceedingJoinPoint->getArguments();
                $this->register($name, $host, $port, $metadata);
                break;
            case 'isRegistered':
                $res = $proceedingJoinPoint->process();
                if ($res) {
                    //已经注册了
                    [$name, $host, $port, $metadata] = $proceedingJoinPoint->getArguments();
                    $protocol = $metadata['protocol'];
                    RegisteredServiceData::getInstance()->add($protocol, $name, $host, $port);
                }
                return $res;
        }
        return true;
    }


    public function register(string $name, string $host, int $port, array $metadata): void
    {
        $protocol                       = $metadata['protocol'];
        $nextId                         = empty($metadata['id']) ? $this->generateId(
            $this->getLastServiceId($name)
        ) : $metadata['id'];
        $deregisterCriticalServiceAfter = $this->config->get(
            'services.drivers.consul.check.deregister_critical_service_after'
        ) ?? '90m';
        $interval                       = $this->config->get('services.drivers.consul.check.interval') ?? '1s';
        $timeout                        = $this->config->get('services.drivers.consul.check.timeout') ?? '1s';
        $requestBody                    = [
            'Name'    => $name,
            'ID'      => $nextId,
            'Address' => $host,
            'Port'    => $port,
            'Meta'    => [
                'Protocol' => $protocol,
            ]
        ];

        switch ($protocol) {
            case JsonRpcHttp::SERVER_NAME:
                try {
                    $requestBody['Check'] = [
                        'DeregisterCriticalServiceAfter' => $deregisterCriticalServiceAfter,
                        'HTTP'                           => "http://{$host}:{$port}/",
                        'Interval'                       => $interval,
                        'Timeout'                        => $timeout,
                        'Body'                           => json_encode([
                            'name'     => $name,
                            'protocol' => $protocol,
                        ], JSON_THROW_ON_ERROR)
                    ];
                } catch (JsonException $e) {
                    logger()->error($e->getMessage());
                }
                break;
            case  Grpc::SERVER_NAME:
                $requestBody['Check'] = [
                    'DeregisterCriticalServiceAfter' => $deregisterCriticalServiceAfter,
                    'GRPC'                           => $host . ':' . $port . Grpc::HEALTHCHECK_ROUTER,
                    'Interval'                       => $interval,
                    'Timeout'                        => $timeout,
                ];
                break;
            default:
                if (in_array($protocol, ['jsonrpc', 'jsonrpc-tcp-length-check', 'multiplex.default'], true)) {
                    $requestBody['Check'] = [
                        'DeregisterCriticalServiceAfter' => $deregisterCriticalServiceAfter,
                        'TCP'                            => "{$host}:{$port}",
                        'Interval'                       => $interval,
                    ];
                }
        }

        $response = $this->client()->registerService($requestBody);
        if ($response->getStatusCode() === 200) {
            $this->registeredServices[$name][$protocol][$host][$port] = true;
            RegisteredServiceData::getInstance()->add($protocol, $name, $host, $port);
            $this->logger->info(sprintf('Service %s:%s register to the consul successfully.', $name, $nextId));
        } else {
            $this->logger->warning(sprintf('Service %s register to the consul failed.', $name));
        }
    }


    protected function client(): AgentInterface
    {
        return $this->container->get(ConsulAgent::class);
    }

    protected function generateId(string $name)
    {
        $exploded  = explode('-', $name);
        $lastIndex = count($exploded) - 1;
        if ($lastIndex > 0 && is_numeric($exploded[$lastIndex])) {
            $exploded[$lastIndex] = ((int)($exploded[$lastIndex])) + 1;
        } else {
            $exploded[] = 0;
        }
        return implode('-', $exploded);
    }

    protected function getLastServiceId(string $name)
    {
        $maxId       = -1;
        $lastService = $name;
        $services    = $this->client()->services()->json();
        foreach ($services ?? [] as $id => $service) {
            if (!isset($service['Service']) || $service['Service'] !== $name) {
                continue;
            }
            $exploded  = explode('-', (string)$id);
            $lastIndex = count($exploded) - 1;
            if ($lastIndex > 0 && is_numeric($exploded[$lastIndex]) && $maxId < $exploded[$lastIndex]) {
                $maxId       = $exploded[$lastIndex];
                $lastService = $service;
            }
        }
        return $lastService['ID'] ?? $name;
    }

}