<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use JsonException;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Throwable;
use Zct\Utils\Data\ModelJsonFieldData;

class DbWriteAspect extends AbstractAspect
{
    public array $classes = [
        'Hyperf\Database\Model\Builder::update',
        'Hyperf\Database\Model\Builder::__call'
    ];

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        try {
            if ($proceedingJoinPoint->methodName === 'update') {
                $this->jsonTransform($proceedingJoinPoint, $proceedingJoinPoint->arguments['keys']['values']);
            } elseif ($proceedingJoinPoint->arguments['keys']['method'] === 'insert') {
                $this->jsonTransform(
                    $proceedingJoinPoint,
                    $proceedingJoinPoint->arguments['keys']['parameters'][0]
                );
            }
        } catch (Throwable) {
        }
        return $proceedingJoinPoint->process();
    }

    private function jsonTransform(ProceedingJoinPoint $proceedingJoinPoint, array &$data)
    {
        $model  = $proceedingJoinPoint->getInstance()->getModel();
        $fields = ModelJsonFieldData::getFields($model);
        foreach ($fields as $field) {
            if (isset($data[$field]) && is_array($data[$field])) {
                try {
                    $data[$field] = json_encode($data[$field], JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR);
                } catch (JsonException $e) {
                    logger()->error($e->getMessage());
                }
            }
        }
    }
}