<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use Hyperf\Di\Aop\ProceedingJoinPoint;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zct\Utils\Data\RegisteredServiceData;
use Zct\Utils\Exception\JsonRpc\HealthCheckException;

class JsonRpcHttpServerAspect
{

    public array $classes = [
        'Hyperf\JsonRpc\HttpServer::initRequestAndResponse',
    ];

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $res = $proceedingJoinPoint->process();

        /**
         * @var RequestInterface $psr7Request
         * @var ResponseInterface $psr7Response
         */
        [$psr7Request] = $res;

        if ($this->isHealthCheck($psr7Request)) {
            $body  = $psr7Request->getBody()->getContents();
            $check = false;
            if ($body) {
                try {
                    $body  = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
                    $check = RegisteredServiceData::getInstance()->check($body['protocol'], $body['name']);
                } catch (Throwable $throwable) {
                    logger()->debug($throwable->getMessage());
                }
            }
            if (!$check) {
                //验证的服务不存在，抛出异常给到 ExceptionHandlerDispatcherAspect 去处理
                throw new HealthCheckException('验证失败：' . $psr7Request->getBody()->getContents(), 410);
            }
        }
        return $res;
    }

    protected function isHealthCheck(RequestInterface $request): bool
    {
        return $request->getHeaderLine('user-agent') === 'Consul Health Check';
    }

}