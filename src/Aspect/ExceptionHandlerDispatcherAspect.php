<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use Hyperf\Context\Context;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zct\Utils\Exception\JsonRpc\HealthCheckException;

class ExceptionHandlerDispatcherAspect
{
    public array $classes = [
        'Hyperf\ExceptionHandler\ExceptionHandlerDispatcher::dispatch',
    ];

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        try {
            $arguments = $proceedingJoinPoint->getArguments();
            [$throwable] = $arguments[0];
            if ($throwable instanceof HealthCheckException) {
                /**
                 * @var ResponseInterface $response
                 */
                $response = Context::get(ResponseInterface::class);
                return $response->withStatus($throwable->getCode(), $throwable->getMessage());
            }
        } catch (Throwable) {
        }
        return $proceedingJoinPoint->process();
    }
}