<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use Co\Redis;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Throwable;
use Zct\Utils\Annotation\ServiceEvent;
use Zct\Utils\Constants\EventConstants;
use Zct\Utils\Data\ServiceEventDispatchData;
use Zct\Utils\Log;
use Zct\Utils\UtilRedis;

/**
 * 跨服务事件调用切面
 */
class EventDispatchAspect extends AbstractAspect
{
    /**
     * @var Redis
     */
    private $redis;

    // 要切入的类或 Trait，可以多个，亦可通过 :: 标识到具体的某个方法，通过 * 可以模糊匹配
    public array $classes = [
        'Hyperf\Event\EventDispatcher::dispatch'
    ];

    public function __construct()
    {
        $this->redis = UtilRedis::get();
        if ($this->redis) {
            $this->eventCollect();
        }
    }

    /**
     * 跨服务事件收集
     */
    public function eventCollect()
    {
        $annotations = AnnotationCollector::getClassesByAnnotation(ServiceEvent::class);
        foreach ($annotations as $class => $annotation) {
            ServiceEventDispatchData::$events[$class] = true;
        }
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $result = $proceedingJoinPoint->process();
        if (!$this->redis) {
            return $result;
        }
        try {
            [$event] = $proceedingJoinPoint->getArguments();
            $class = get_class($event);
            if (isset(ServiceEventDispatchData::$events[$class])) {
                $this->redis->publish(
                    EventConstants::SERVICE_EVENT_PR . $class,
                    json_encode(get_object_vars($event), JSON_THROW_ON_ERROR)
                );
            }
        } catch (Throwable $throwable) {
            Log::error('EventDispatch process:' . $throwable->getMessage());
        }
        return $result;
    }
}