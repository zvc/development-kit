<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\HttpServer\Router\Router;
use Hyperf\ServiceGovernance\ServiceManager;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Zct\Utils\Annotation\GrpcService;
use Zct\Utils\Constants\Grpc;
use Zct\Utils\Grpc\Container\Container;
use Zct\Utils\Grpc\Container\Method;
use Zct\Utils\Grpc\Container\Server;
use Zct\Utils\Grpc\Server\Health;

use function Hyperf\Config\config;


class DispatcherFactoryAspec
{
    public array $classes = [
        'Hyperf\HttpServer\Router\DispatcherFactory::initConfigRoute',
    ];

    protected ContainerInterface $container;

    protected ConfigInterface $config;

    protected StdoutLoggerInterface $logger;

    private ServiceManager $serviceManager;

    public function __construct(ContainerInterface $container)
    {
        $this->container      = $container;
        $this->logger         = $container->get(StdoutLoggerInterface::class);
        $this->config         = $container->get(ConfigInterface::class);
        $this->serviceManager = $container->get(ServiceManager::class);;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $result  = $proceedingJoinPoint->process();
        $servers = config('server.servers');
        if (!$servers) {
            return $result;
        }
        $hasGrpc = false;
        foreach ($servers as $server) {
            if ($server['name'] === Grpc::SERVER_NAME) {
                $hasGrpc = true;
                break;
            }
        }
        if (!$hasGrpc) {
            return $result;
        }
        $this->setGrpcAutoRouter();
        $this->setCheckRouter();
        return $result;
    }

    private function setGrpcAutoRouter()
    {
        $classes = AnnotationCollector::getClassesByAnnotation(GrpcService::class);
        if (!count($classes)) {
            return;
        }
        Router::addServer(Grpc::SERVER_NAME,  static function () use ($classes) {
            /**
             * @var GrpcService $service
             */
            foreach ($classes as $className => $service) {
                if ($service->name === '') {
                    $classNameArr  = explode('\\', $className);
                    $service->name = end($classNameArr);
                }

                $classRef = new ReflectionClass($className);

                $serverInfo            = new Server();
                $serverInfo->name      = $service->name;
                $serverInfo->package   = $service->package;
                $serverInfo->publishTo = $service->publishTo;

                foreach ($classRef->getMethods() as $method) {
                    if ($method->isPublic() && $method->getName() !== '__construct') {
                        $routerData     = Router::getData();
                        $postRouterData = [];
                        if (isset($routerData[0])) {
                            $postRouterData = Router::getData()[0]['POST'] ?? [];
                        }

                        $methodInfo          = new Method();
                        $methodInfo->name    = $method->getName();
                        $methodInfo->handler = $method->getDeclaringClass()->getName() . '@' . $methodInfo->name;
                        $methodInfo->path    = '/' . $serverInfo->package . '.' . $service->name . '/' . $methodInfo->name;

                        $serverInfo->methods[$methodInfo->path] = $methodInfo;

                        if (!isset($postRouterData[$methodInfo->path])) {
                            // 已经配置过的地址不能重复配置
                            Router::addGroup(
                                '/' . $serverInfo->package . '.' . $service->name,
                                static function () use ($methodInfo) {
                                    Router::post(
                                        '/' . $methodInfo->name,
                                        $methodInfo->handler
                                    );
                                }
                            );
                        }
                    }
                }

                if (count($serverInfo->methods)) {
                    Container::add($serverInfo);
                }
            }
        });
    }

    private function setCheckRouter()
    {
        $grpcServers = Container::getAll();
        if (!count($grpcServers)) {
            return;
        }
        Router::addServer(Grpc::SERVER_NAME, function () {
            $routerData = Router::getData();
            if (!isset($routerData[0]['POST']) || count($routerData[0]['POST']) < 1) {
                return;
            }
            $postRouterData = $routerData[0]['POST'];
            if (!isset($postRouterData[Grpc::HEALTHCHECK_ROUTER])) {
                Router::addGroup('/' . Grpc::HEALTHCHECK_SERVICE, function () {
                    Router::post('/Check', Health::class . '@Check');
                });
            }
        });
    }
}