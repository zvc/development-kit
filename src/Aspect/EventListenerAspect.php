<?php

declare(strict_types=1);

namespace Zct\Utils\Aspect;

use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Event\ListenerData;
use Zct\Utils\Annotation\ServiceEvent;
use Zct\Utils\Data\ServiceEventListenerData;

/**
 * 跨服务事件监听器切面
 */
class EventListenerAspect extends AbstractAspect
{
    // 要切入的类或 Trait，可以多个，亦可通过 :: 标识到具体的某个方法，通过 * 可以模糊匹配
    public array $classes = [
        'Hyperf\Event\ListenerProvider::on'
    ];

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        [$event, $listener, $priority] = $proceedingJoinPoint->getArguments();
        if (AnnotationCollector::getClassAnnotation($event, ServiceEvent::class)) {
            //跨服务事件
            ServiceEventListenerData::$listenerDatas[] = new ListenerData($event, $listener, $priority);
            $result                                    = true;
        } else {
            $result = $proceedingJoinPoint->process();
        }
        return $result;
    }
}