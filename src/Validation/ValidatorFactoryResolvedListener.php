<?php

declare(strict_types=1);

namespace Zct\Utils\Validation;

use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Validation\Event\ValidatorFactoryResolved;

class ValidatorFactoryResolvedListener implements ListenerInterface
{

    public function listen(): array
    {
        return [
            ValidatorFactoryResolved::class,
        ];
    }

    public function process(object $event): void
    {
        /**  @var ValidatorFactoryInterface $validatorFactory */
        $validatorFactory = $event->validatorFactory;
        // 注册验证器
        $validatorFactory->extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match("/^1[3456789]\d{9}$/", $value) > 0;
        });
        $validatorFactory->extend('tel', function ($attribute, $value, $parameters, $validator) {
            return preg_match("/^([0-9]{3,4}-)?[0-9]{7,8}$/", $value) > 0;
        });

        // 当创建一个自定义验证规则时，你可能有时候需要为错误信息定义自定义占位符
        $validatorFactory->replacer('phone', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':phone', $attribute, $message);
        });
        $validatorFactory->replacer('tel', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':tel', $attribute, $message);
        });
    }
}