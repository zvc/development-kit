<?php

declare(strict_types=1);

namespace Zct\Utils\Validation;

use RuntimeException;

class ValidationException extends RuntimeException
{
}