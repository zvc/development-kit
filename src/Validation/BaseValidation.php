<?php

declare(strict_types=1);

namespace Zct\Utils\Validation;

use Hyperf\Context\ApplicationContext;

class BaseValidation
{
    protected static array $rules            = [];
    protected static array $messages         = [];
    protected static array $customAttributes = [];

    /**
     * @param array $requireds
     * @param array $rules
     * @return array
     */
    public static function getRules(array $requireds = [], array $rules = []): array
    {
        $rules = array_merge(static::$rules, $rules);
        foreach ($requireds as $field) {
            if (isset($rules[$field])) {
                $rules[$field] = 'required|' . $rules[$field];
            } else {
                $rules[$field] = 'required';
            }
        }
        return $rules;
    }

    /**
     * @param array $data
     * @param array $requireds
     * @param array $rules
     * @return bool
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function check(array $data, array $requireds = [], array $rules = []): bool
    {
        $validatorFactory = ApplicationContext::getContainer()->get(
            'Hyperf\Validation\Contract\ValidatorFactoryInterface'
        );
        $validator        = $validatorFactory->make(
            $data,
            static::getRules($requireds, $rules),
            static::$messages,
            static::$customAttributes
        );
        if ($validator->fails()) {
            throw new ValidationException($validator->errors()->first());
        }
        return true;
    }
}