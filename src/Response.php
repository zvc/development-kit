<?php

declare(strict_types=1);

namespace Zct\Utils;

use JsonException;
use Throwable;
use Zct\Utils\Interfaces\ResponseInterface;

/**
 * 响应数据
 * Class Response
 * @package Zct\Utils
 */
final class Response implements ResponseInterface
{
    public string $msg     = '';
    public array  $data    = [];
    public int    $code    = 0;
    public int    $errCode = 0;
    public string $service = '';

    public function __construct(
        string $msg = '',
        array $data = [],
        int $code = 0,
        int $errCode = 0,
        string $service = ''
    ) {
        $this->msg     = $msg;
        $this->data    = $data;
        $this->code    = $code;
        $this->errCode = $errCode;
        $this->service = $service;
    }

    public function set(string $msg = '', array $data = [], int $code = 0, int $errCode = 0, string $service = ''): self
    {
        $this->msg     = $msg;
        $this->data    = $data;
        $this->code    = $code;
        $this->errCode = $errCode;
        $this->service = $service;
        return $this;
    }

    public function success(string $msg = '', array $data = [], int $code = 1): self
    {
        return $this->set($msg, $data, $code);
    }

    public function error(string $msg = '', array $data = [], int $errCode = 0): self
    {
        return $this->set($msg, $data, 0, $errCode);
    }

    public function json(): string
    {
        try {
            return json_encode([
                'code'    => $this->code,
                'msg'     => $this->msg,
                'errCode' => $this->errCode,
                'data'    => $this->data,
                'service' => $this->service,
            ], JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
        } catch (JsonException) {
            return '';
        }
    }

    public static function unpack(string|array $packData): self
    {
        $self = new self();
        if (is_string($packData)) {
            try {
                $packData = json_decode($packData, true, 512, JSON_THROW_ON_ERROR);
            } catch (Throwable) {
                $packData = [];
            }
        }
        foreach ($packData as $k => $v) {
            $self->{$k} = $v;
        }
        return $self;
    }
}