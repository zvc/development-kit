<?php

declare(strict_types=1);

use Hyperf\Context\ApplicationContext;
use Hyperf\Contract\StdoutLoggerInterface;
use Psr\Log\LoggerInterface;
use Zct\Utils\Db;
use Zct\Utils\Interfaces\ErrCodeEnumInterface;
use Zct\Utils\Interfaces\RequestInterface;
use Zct\Utils\Interfaces\ResponseInterface;
use Zct\Utils\Log;
use Zct\Utils\Request;
use Zct\Utils\Response;
use function Hyperf\Coroutine\go;
use function Hyperf\Support\env;
use function Hyperf\Support\make;

if (!function_exists('error')) {
    /**
     * @param int|ErrCodeEnumInterface $errCode
     * @param string $msg
     * @param array $data
     * @return ResponseInterface
     */
    function error(int|ErrCodeEnumInterface $errCode = 1, string $msg = '操作失败', array $data = []): ResponseInterface
    {
        $response          = make(Response::class, ['enableCache' => true]);
        $response->code    = 0;
        $response->errCode = (int)$errCode;
        $response->msg     = $msg;
        $response->data    = $data;
        $response->service = env('APP_NAME', '');
        return $response;
    }
}

if (!function_exists('success')) {
    /**
     * @param string $msg
     * @param array $data
     * @param int $code
     * @return ResponseInterface
     */
    function success(string $msg = '操作成功', array $data = [], int $code = 1): ResponseInterface
    {
        $response          = make(Response::class, ['enableCache' => true]);
        $response->code    = $code;
        $response->errCode = 0;
        $response->msg     = $msg;
        $response->data    = $data;
        $response->service = env('APP_NAME', '');
        return $response;
    }
}

if (!function_exists('rpcRequest')) {
    function rpcRequest(): RequestInterface
    {
        try {
            if (ApplicationContext::getContainer()->has('Hyperf\Rpc\Context')) {
                $rpc = ApplicationContext::getContainer()->get('Hyperf\Rpc\Context');
                if ($packData = $rpc->get('internalRequest')) {
                    return Request::unpack($packData);
                }
            }
        } catch (Throwable) {
        }
        return Request::getInstance();
    }
}

if (!function_exists('setRpcRequest')) {
    function setRpcRequest($key, $value = null): bool
    {
        try {
            $rpcRequest = rpcRequest();
            if (is_array($key)) {
                foreach ($key as $k => $v) {
                    $rpcRequest->set($k, $v);
                }
            } elseif (is_string($key)) {
                $rpcRequest->set($key, $value);
            } else {
                return false;
            }
            if (ApplicationContext::getContainer()->has('Hyperf\Rpc\Context')) {
                $rpc = ApplicationContext::getContainer()->get('Hyperf\Rpc\Context');
                $rpc->set('internalRequest', $rpcRequest->pack());
                return true;
            }
        } catch (Throwable $throwable) {
            Log::error('setRpcRequest:' . $throwable->getMessage());
        }
        return false;
    }
}

if (!function_exists('internalRequest')) {
    function internalRequest(): RequestInterface
    {
        return rpcRequest();
    }
}

if (!function_exists('input')) {
    /**
     * 获取rpc上下文中的输入数据
     * @param string $field
     * @param null $default
     * @return mixed
     */
    function input(string $field = '', $default = null): mixed
    {
        $param = internalRequest()->params;
        if ($field === '') {
            return $param ?: null;
        }
        // 解析field
        if (strpos($field, '/')) {
            [$field, $type] = explode('/', $field);
        }
        $data = $param[$field] ?? null;
        if (is_null($data)) {
            return $default;
        }
        if (is_object($data)) {
            return $data;
        }
        if (isset($type) && $data !== $default) {
            // 强制类型转换
            switch (strtolower($type)) {
                // 数组
                case 'a':
                    $data = (array)$data;
                    break;
                // 数字
                case 'd':
                    $data = (int)$data;
                    break;
                // 浮点
                case 'f':
                    $data = (float)$data;
                    break;
                // 布尔
                case 'b':
                    $data = (boolean)$data;
                    break;
                // 字符串
                case 's':
                    if (is_scalar($data)) {
                        $data = (string)$data;
                    } else {
                        throw new InvalidArgumentException('variable type error：' . gettype($data));
                    }
                    break;
            }
        }
        return $data;
    }
}

if (!function_exists('Model')) {
    function Model(string $class): Db
    {
        return Db::Model($class);
    }
}

if (!function_exists('logger')) {
    function logger(): LoggerInterface
    {
        return ApplicationContext::getContainer()->get(StdoutLoggerInterface::class);
    }
}

if (!function_exists('sgo')) {
    function sgo(callable $callable): bool|int
    {
        if (ApplicationContext::getContainer()->has('Hyperf\Rpc\Context')) {
            $rpc     = ApplicationContext::getContainer()->get('Hyperf\Rpc\Context');
            $context = $rpc->getData();
            $id      = go(static function () use ($rpc, $context, $callable) {
                $rpc->setData($context);
                $callable();
            });
        } else {
            $id = go(static function () use ($callable) {
                $callable();
            });
        }
        return $id > 0 ? $id : false;
    }
}
