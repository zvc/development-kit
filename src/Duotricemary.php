<?php

declare(strict_types=1);

namespace Zct\Utils;

/**
 * 36进制转换类
 */
class Duotricemary
{
    private static array $charArr        = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z'
    ];
    private static array $charArrFlip    = [
        '0' => 0,
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'a' => 10,
        'b' => 11,
        'c' => 12,
        'd' => 13,
        'e' => 14,
        'f' => 15,
        'g' => 16,
        'h' => 17,
        'i' => 18,
        'j' => 19,
        'k' => 20,
        'l' => 21,
        'm' => 22,
        'n' => 23,
        'o' => 24,
        'p' => 25,
        'q' => 26,
        'r' => 27,
        's' => 28,
        't' => 29,
        'u' => 30,
        'v' => 31,
        'w' => 32,
        'x' => 33,
        'y' => 34,
        'z' => 35
    ];
    private static array $charBigArrFlip = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        'a' => '10',
        'b' => '11',
        'c' => '12',
        'd' => '13',
        'e' => '14',
        'f' => '15',
        'g' => '16',
        'h' => '17',
        'i' => '18',
        'j' => '19',
        'k' => '20',
        'l' => '21',
        'm' => '22',
        'n' => '23',
        'o' => '24',
        'p' => '25',
        'q' => '26',
        'r' => '27',
        's' => '28',
        't' => '29',
        'u' => '30',
        'v' => '31',
        'w' => '32',
        'x' => '33',
        'y' => '34',
        'z' => '35'
    ];

    /**
     * 输入10进制
     * @param int $num
     * @return string
     */
    public static function input10(int $num): string
    {
        $symbol = '';
        if ($num <= 0) {
            $symbol = '-';
            $num    *= -1;
        }
        $char = '';
        do {
            $key  = ($num - 1) % 36;
            $char = self::$charArr[$key] . $char;
            $num  = floor(($num - $key) / 36);
        } while ($num > 0);
        return $symbol . $char;
    }

    /**
     * 输入大数的10进制
     * @param string|int $num
     * @return string
     */
    public static function inputBig10(string|int $num): string
    {
        $symbol = '';
        if ($num <= 0) {
            $symbol = '-';
            $num    = bcmul($num, '-1');
        }
        if ($num <= PHP_INT_MAX) {
            return $symbol . self::input10((int)$num);
        }
        $char = '';
        do {
            $i    = (string)bcmod(bcsub($num, '1'), '36');
            $char = self::$charArr[$i] . $char;
            $num  = bcdiv(bcsub($num, $i), '36');
        } while ($num > 0);
        return $symbol . $char;
    }

    /**
     * 输出10进制
     * @param string $char
     * @return int|string
     */
    public static function output10(string $char): int|string
    {
        $symbol = 1;
        if ($char[0] === '-') {
            $symbol = -1;
            $char   = substr($char, 1);
        }
        if (!$char) {
            return 0;
        }
        if (strlen($char) > 12) {
            //处理大整数
            return ($symbol === 1 ? '' : '-') . self::outputBig10($char);
        }
        $sum = 0;
        $len = strlen($char);
        for ($i = 0; $i < $len; $i++) {
            $sum += (self::$charArrFlip[$char[$i]] + 1) * (36 ** ($len - $i - 1));
        }
        return $sum * $symbol;
    }

    /**
     * 输出大数的10进制
     * @param string $char
     * @return string
     */
    public static function outputBig10(string $char): string
    {
        $symbol = '';
        if ($char[0] === '-') {
            $symbol = '-';
            $char   = substr($char, 1);
        }
        if (!$char) {
            return '0';
        }

        $sum = '0';
        $len = (string)strlen($char);
        for ($i = 0; $i < $len; $i++) {
            $sum = bcadd(
                $sum,
                bcmul(bcadd(self::$charBigArrFlip[$char[$i]], '1'), bcpow('36', bcsub(bcsub($len, (string)$i), '1')))
            );
        }
        return $symbol . $sum;
    }
}