<?php

declare(strict_types=1);

namespace Zct\Utils;

use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Contract\PaginatorInterface;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;
use Hyperf\Collection\Collection;
use Throwable;

/**
 * Class Db
 * @package Zct\Utils
 * @method bool chunk($count, callable $callback)
 * @method bool each(callable $callback, $count = 1000)
 * @method null|Model|object first($columns = ['*'])
 * @method $this|mixed when($value, $callback, $default = null)
 * @method $this|mixed tap($callback)
 * @method $this|mixed unless($value, $callback, $default = null)
 * @method \Hyperf\Database\Model\Builder has($relation, $operator = '>=', $count = 1, $boolean = 'and', Closure $callback = null)
 * @method \Hyperf\Database\Model\Builder orHas($relation, $operator = '>=', $count = 1)
 * @method \Hyperf\Database\Model\Builder doesntHave($relation, $boolean = 'and', Closure $callback = null)
 * @method \Hyperf\Database\Model\Builder orDoesntHave($relation)
 * @method \Hyperf\Database\Model\Builder whereHas($relation, Closure $callback = null, $operator = '>=', $count = 1)
 * @method \Hyperf\Database\Model\Builder orWhereHas($relation, Closure $callback = null, $operator = '>=', $count = 1)
 * @method \Hyperf\Database\Model\Builder whereDoesntHave($relation, Closure $callback = null)
 * @method \Hyperf\Database\Model\Builder orWhereDoesntHave($relation, Closure $callback = null)
 * @method $this withCount($relations)
 * @method $this withMax($relation, string $column)
 * @method $this withMin($relation, string $column)
 * @method $this withSum($relation, string $column)
 * @method $this withAvg($relation, string $column)
 * @method $this withAggregate($relations, string $column, $function = null)
 * @method \Hyperf\Database\Model\Builder mergeConstraintsFrom(Builder $from)
 * @method $this whereHasMorph($relation, $types, Closure $callback = null, $operator = '>=', $count = 1)
 * @method $this orWhereHasMorph(string $relation, $types, Closure $callback = null, string $operator = '>=', int $count = 1)
 * @method $this whereDoesntHaveMorph(string $relation, $types, Closure $callback = null)
 * @method $this orWhereDoesntHaveMorph(string $relation, $types, Closure $callback = null)
 * @method $this hasMorph($relation, $types, $operator = '>=', $count = 1, $boolean = 'and', Closure $callback = null)
 * @method $this doesntHaveMorph(string $relation, $types, $boolean = 'and', Closure $callback = null)
 * @method \Hyperf\Database\Model\Model make(array $attributes = [])
 * @method $this withGlobalScope($identifier, $scope)
 * @method $this withoutGlobalScope($scope)
 * @method $this withoutGlobalScopes(array $scopes = null)
 * @method array removedScopes()
 * @method $this whereKey($id)
 * @method $this whereKeyNot($id)
 * @method \Hyperf\Database\Model\Builder orWhere($column, $operator = null, $value = null)
 * @method $this latest($column = null)
 * @method $this oldest($column = null)
 * @method \Hyperf\Database\Model\Collection hydrate(array $items)
 * @method \Hyperf\Database\Model\Collection fromQuery($query, $bindings = [])
 * @method null|\Hyperf\Database\Model\Collection|\Hyperf\Database\Model\Model find($id, $columns = ['*'])
 * @method \Hyperf\Database\Model\Collection findMany($ids, $columns = ['*'])
 * @method \Hyperf\Database\Model\Collection|\Hyperf\Database\Model\Model findOrFail($id, $columns = ['*'])
 * @method \Hyperf\Database\Model\Model findOrNew($id, $columns = ['*'])
 * @method \Hyperf\Database\Model\Model firstOrNew(array $attributes, array $values = [])
 * @method \Hyperf\Database\Model\Model firstOrCreate(array $attributes, array $values = [])
 * @method \Hyperf\Database\Model\Model firstOrFail($columns = ['*'])
 * @method \Hyperf\Database\Model\Model updateOrCreate(array $attributes, array $values = [])
 * @method \Hyperf\Database\Model\Model|mixed firstOr($columns = ['*'], Closure $callback = null)
 * @method value($column)
 * @method \Hyperf\Database\Model\Collection get($columns = ['*'])
 * @method \Hyperf\Database\Model\Model[] getModels($columns = ['*'])
 * @method array eagerLoadRelations(array $models)
 * @method \Hyperf\Database\Model\Relations\Relation getRelation($name)
 * @method \Generator cursor()
 * @method bool chunkById($count, callable $callback, $column = null, $alias = null)
 * @method \Hyperf\Collection\Collection pluck($column, $key = null)
 * @method \Hyperf\Contract\PaginatorInterface simplePaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
 * @method $this|\Hyperf\Database\Model\Model create(array $attributes = [])
 * @method $this|\Hyperf\Database\Model\Model forceCreate(array $attributes)
 * @method int update(array $values)
 * @method int  increment($column, $amount = 1, array $extra = [])
 * @method int  decrement($column, $amount = 1, array $extra = [])
 * @method delete()
 * @method forceDelete()
 * @method onDelete(Closure $callback)
 * @method scopes(array $scopes)
 * @method \Hyperf\Database\Model\Builder applyScopes()
 * @method $this with($relations)
 * @method $this without($relations)
 * @method \Hyperf\Database\Model\Model newModelInstance($attributes = [])
 * @method $this withCasts($casts)
 * @method $this setQuery($query)
 * @method \Hyperf\Database\Query\Builder toBase()
 * @method array getEagerLoads()
 * @method $this setEagerLoads(array $eagerLoad)
 * @method \Hyperf\Database\Model\Model getModel()
 * @method $this setModel(Model $model)
 * @method string qualifyColumn($column)
 * @method \Closure getMacro($name)
 * @method \Hyperf\Database\Query\Builder selectRaw($expression, array $bindings = [])
 * @method \Hyperf\Database\Query\Builder fromRaw($expression, $bindings = [])
 * @method \Hyperf\Database\Query\Builder whereRaw($sql, $bindings = [], $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereRaw($sql, $bindings = [])
 * @method \Hyperf\Database\Query\Builder whereIntegerInRaw($column, $values, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder whereIntegerNotInRaw($column, $values, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder havingRaw($sql, array $bindings = [], $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orHavingRaw($sql, array $bindings = [])
 * @method \Hyperf\Database\Query\Builder orderByRaw($sql, $bindings = [])
 * @method \Hyperf\Database\Query\Expression raw($value)
 * @method array getRawBindings()
 * @method \Hyperf\Database\Query\Builder select($columns = ['*'])
 * @method \Hyperf\Database\Query\Builder selectSub($query, $as)
 * @method \Hyperf\Database\Query\Builder fromSub($query, $as)
 * @method \Hyperf\Database\Query\Builder addSelect($column)
 * @method \Hyperf\Database\Query\Builder distinct()
 * @method \Hyperf\Database\Query\Builder from($table)
 * @method \Hyperf\Database\Query\Builder join($table, $first, $operator = null, $second = null, $type = 'inner', $where = false)
 * @method \Hyperf\Database\Query\Builder joinWhere($table, $first, $operator, $second, $type = 'inner')
 * @method \Hyperf\Database\Query\Builder joinSub($query, $as, $first, $operator = null, $second = null, $type = 'inner', $where = false)
 * @method \Hyperf\Database\Query\Builder leftJoin($table, $first, $operator = null, $second = null)
 * @method \Hyperf\Database\Query\Builder leftJoinWhere($table, $first, $operator, $second)
 * @method \Hyperf\Database\Query\Builder leftJoinSub($query, $as, $first, $operator = null, $second = null)
 * @method \Hyperf\Database\Query\Builder rightJoin($table, $first, $operator = null, $second = null)
 * @method \Hyperf\Database\Query\Builder rightJoinWhere($table, $first, $operator, $second)
 * @method \Hyperf\Database\Query\Builder rightJoinSub($query, $as, $first, $operator = null, $second = null)
 * @method \Hyperf\Database\Query\Builder crossJoin($table, $first = null, $operator = null, $second = null)
 * @method mergeWheres($wheres, $bindings)
 * @method array prepareValueAndOperator($value, $operator, $useDefault = false)
 * @method \Hyperf\Database\Query\Builder whereColumn($first, $operator = null, $second = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereColumn($first, $operator = null, $second = null)
 * @method \Hyperf\Database\Query\Builder whereIn($column, $values, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder orWhereIn($column, $values)
 * @method \Hyperf\Database\Query\Builder whereNotIn($column, $values, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereNotIn($column, $values)
 * @method \Hyperf\Database\Query\Builder whereNull($columns, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder orWhereNull($column)
 * @method \Hyperf\Database\Query\Builder whereNotNull($column, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder whereBetween($column, array $values, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder orWhereBetween($column, array $values)
 * @method \Hyperf\Database\Query\Builder whereNotBetween($column, array $values, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereNotBetween($column, array $values)
 * @method \Hyperf\Database\Query\Builder orWhereNotNull($column)
 * @method \Hyperf\Database\Query\Builder whereDate($column, $operator, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereDate($column, $operator, $value = null)
 * @method \Hyperf\Database\Query\Builder whereTime($column, $operator, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereTime($column, $operator, $value = null)
 * @method \Hyperf\Database\Query\Builder whereDay($column, $operator, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereDay($column, $operator, $value = null)
 * @method \Hyperf\Database\Query\Builder whereMonth($column, $operator, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereMonth($column, $operator, $value = null)
 * @method \Hyperf\Database\Query\Builder whereYear($column, $operator, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereYear($column, $operator, $value = null)
 * @method \Hyperf\Database\Query\Builder whereNested(Closure $callback, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder forNestedWhere()
 * @method \Hyperf\Database\Query\Builder addNestedWhereQuery($query, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder whereExists(Closure $callback, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder orWhereExists(Closure $callback, $not = false)
 * @method \Hyperf\Database\Query\Builder whereNotExists(Closure $callback, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereNotExists(Closure $callback)
 * @method \Hyperf\Database\Query\Builder addWhereExistsQuery(self $query, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder whereRowValues($columns, $operator, $values, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereRowValues($columns, $operator, $values)
 * @method \Hyperf\Database\Query\Builder whereJsonContains($column, $value, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder orWhereJsonContains($column, $value)
 * @method \Hyperf\Database\Query\Builder whereJsonDoesntContain($column, $value, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereJsonDoesntContain($column, $value)
 * @method \Hyperf\Database\Query\Builder whereJsonLength($column, $operator, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orWhereJsonLength($column, $operator, $value = null)
 * @method \Hyperf\Database\Query\Builder dynamicWhere($method, $parameters)
 * @method \Hyperf\Database\Query\Builder groupBy(...$groups)
 * @method \Hyperf\Database\Query\Builder having($column, $operator = null, $value = null, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder orHaving($column, $operator = null, $value = null)
 * @method \Hyperf\Database\Query\Builder havingBetween($column, array $values, $boolean = 'and', $not = false)
 * @method \Hyperf\Database\Query\Builder orderBy($column, $direction = 'asc')
 * @method \Hyperf\Database\Query\Builder orderByDesc($column)
 * @method \Hyperf\Database\Query\Builder inRandomOrder($seed = '')
 * @method \Hyperf\Database\Query\Builder skip($value)
 * @method \Hyperf\Database\Query\Builder offset($value)
 * @method \Hyperf\Database\Query\Builder take($value)
 * @method \Hyperf\Database\Query\Builder limit($value)
 * @method \Hyperf\Database\Query\Builder forPage($page, $perPage = 15)
 * @method \Hyperf\Database\Query\Builder forPageBeforeId($perPage = 15, $lastId = 0, $column = 'id')
 * @method \Hyperf\Database\Query\Builder forPageAfterId($perPage = 15, $lastId = 0, $column = 'id')
 * @method \Hyperf\Database\Query\Builder union($query, $all = false)
 * @method \Hyperf\Database\Query\Builder unionAll($query)
 * @method \Hyperf\Database\Query\Builder lock($value = true)
 * @method \Hyperf\Database\Query\Builder lockForUpdate()
 * @method \Hyperf\Database\Query\Builder sharedLock()
 * @method string toSql()
 * @method int getCountForPagination($columns = ['*'])
 * @method string implode($column, $glue = '')
 * @method bool exists()
 * @method bool doesntExist()
 * @method int count($columns = '*')
 * @method min($column)
 * @method max($column)
 * @method sum($column)
 * @method avg($column)
 * @method average($column)
 * @method aggregate($function, $columns = ['*'])
 * @method float|int numericAggregate($function, $columns = ['*'])
 * @method bool insert(array $values)
 * @method int insertGetId(array $values, $sequence = null)
 * @method bool insertUsing(array $columns, $query)
 * @method int insertOrIgnore(array $values)
 * @method bool updateOrInsert(array $attributes, array $values = [])
 * @method truncate()
 * @method \Hyperf\Database\Query\Builder newQuery()
 * @method array getBindings()
 * @method \Hyperf\Database\Query\Builder setBindings(array $bindings, $type = 'where')
 * @method \Hyperf\Database\Query\Builder addBinding($value, $type = 'where')
 * @method \Hyperf\Database\Query\Builder mergeBindings(\Hyperf\Database\Query\Builder $query)
 * @method \Hyperf\Database\ConnectionInterface getConnection()
 * @method \Hyperf\Database\Query\Processors\Processor getProcessor()
 * @method \Hyperf\Database\Query\Grammars\Grammar getGrammar()
 * @method \Hyperf\Database\Query\Builder useWritePdo()
 * @method array createSub($query)
 * @method array parseSub($query)
 * @method \Hyperf\Database\Query\Builder addArrayOfWheres($column, $boolean, $method = 'where')
 * @method bool invalidOperatorAndValue($operator, $value)
 * @method bool invalidOperator($operator)
 * @method \Hyperf\Database\Query\Builder whereInSub($column, Closure $callback, $boolean, $not)
 * @method \Hyperf\Database\Query\Builder whereInExistingQuery($column, $query, $boolean, $not)
 * @method \Hyperf\Database\Query\Builder addDateBasedWhere($type, $column, $operator, $value, $boolean = 'and')
 * @method \Hyperf\Database\Query\Builder whereSub($column, $operator, Closure $callback, $boolean)
 * @method addDynamic($segment, $connector, $parameters, $index)
 * @method array removeExistingOrdersFor($column)
 * @method array runSelect()
 * @method \Hyperf\Database\Query\Builder cloneForPaginationCount()
 * @method array runPaginationCountQuery($columns = ['*'])
 * @method array withoutSelectAliases(array $columns)
 * @method enforceOrderBy()
 * @method null|string stripTableForPluck($column)
 * @method \Hyperf\Collection\Collection pluckFromObjectColumn($queryResult, $column, $key)
 * @method \Hyperf\Collection\Collection pluckFromArrayColumn($queryResult, $column, $key)
 * @method \Hyperf\Database\Query\Builder setAggregate($function, $columns)
 * @method onceWithColumns($columns, $callback)
 * @method \Hyperf\Database\Query\Builder forSubQuery()
 * @method array cleanBindings(array $bindings)
 * @method LengthAwarePaginatorInterface paginator(Collection $items, int $total, int $perPage, int $currentPage, array $options)
 * @method PaginatorInterface simplePaginator(Collection $items, int $perPage, int $currentPage, array $options)
 * @method mixed assertBinding($value, $column = '', int $limit = 0)
 */
final class Db
{
    private array  $fields   = ['*'];
    private ?array $dbFields = null;

    public function __construct(private Builder $query)
    {
        try {
            $this->dbFields = $query->getModel()::FIELDS ?? ['*'];
        } catch (Throwable) {
        }
    }

    public function __call($method, $parameters)
    {
        $res = call_user_func_array([$this->query, $method], $parameters);
        if ($res instanceof Builder) {
            $this->query = $res;
            return $this;
        }
        return $res;
    }

    public static function Model(string $class): self
    {
        return new self(call_user_func([$class, 'query']));
    }

    public function getQuery(): Builder
    {
        return $this->query;
    }

    public function filterSensitiveCharacter(string &$string): string
    {
        $pattern = '/[a-zA-Z0-9_|>\-]/u';
        preg_match_all($pattern, $string, $result);
        $string = implode('', $result[0]);
        return $string;
    }

    public function field(array $fields = ['*']): self
    {
        $this->fields = $fields;
        return $this;
    }

    public function where($column, $operator = null, $value = null, $boolean = 'and'): self
    {
        if (!is_array($column)) {
            $this->query->where(...func_get_args());
        } else {
            $this->_superWhere($column);
        }
        return $this;
    }

    /**
     * 判断是否是允许的字段
     * @param string $field
     * @return bool
     */
    private function _isAllowField(string $field): bool
    {
        return (
            !$this->dbFields ||
            isset($this->dbFields[$field]) ||
            isset($this->dbFields[explode('->', $field)[0]])
        );
    }

    private function _superWhere(array $where): self
    {
        $whereMethods = ['in', 'notin', 'between', 'notbetween'];
        $operators    = ['=', '<', '>', '<=', '>=', '<>', '!=', '<=>', 'like', 'not like'];
        $whereTrim    = [];
        foreach ($where as $key => $item) {
            if (is_string($key)) {
                //将where条件全部转成二维数组形式[[$column, $operator, $value]]
                if (is_array($item)) {
                    [$operator, $value] = $item;
                    $whereTrim[] = [$key, $operator, $value];
                } else {
                    $whereTrim[] = [$key, '=', $item];
                }
                continue;
            }
            //到这边的key全部为int类型
            if (!is_array($item)) {
                continue;
            }
            $whereMethod = 'where';
            if (count($item) > 2) {
                [$column, $operator, $value] = $item;
                $operator = strtolower($operator);
                if (!is_callable([$this->query, $operator]) &&
                    !in_array(
                        $operator,
                        array_merge($operators, $whereMethods),
                        true
                    )
                ) {
                    continue;
                }
            } else {
                [$column, $value] = $item;
                $operator = '=';
            }
            if (in_array($operator, $whereMethods, true)) {
                $whereMethod = 'where' . $operator;
                $operator    = $value;
                $value       = 'and';
            }
            $this->filterSensitiveCharacter($column);
            if (str_contains($column, '|')) {
                //多个字段查询同一个条件
                if ($whereMethod === 'where') {
                    $whereMethod = 'orwhere';
                } else {
                    $value = 'or';
                }
                $columns = explode('|', $column);
                $this->query->where(function ($query) use ($columns, $operator, $value, $whereMethod) {
                    foreach ($columns as $columnSub) {
                        if ($this->fields === ['*'] || in_array($columnSub, $this->fields, true)) {
                            if ($this->_isAllowField($columnSub)) {
                                $query->{$whereMethod}($columnSub, $operator, $value);
                            }
                        }
                    }
                });
            } elseif ($this->fields === ['*'] || in_array($column, $this->fields, true)) {
                if ($this->_isAllowField($column)) {
                    $this->query->{$whereMethod}($column, $operator, $value);
                }
            }
        }
        if (count($whereTrim)) {
            return $this->_superWhere($whereTrim);
        }
        return $this;
    }

    public function order(string $order = ''): self
    {
        if ($order !== '') {
            $order    = preg_replace('/\s(?=\s)/', "\\1", $order);//将连续的空格转为一个
            $orderArr = explode(',', $order);
            if (count($orderArr)) {
                foreach ($orderArr as $orderStrItem) {
                    $data = explode(' ', trim($orderStrItem));
                    $this->filterSensitiveCharacter($data[0]);
                    if ($this->_isAllowField($data[0])) {
                        $this->query->orderBy($data[0], $data[1] ?? 'asc');
                    }
                }
            }
        }
        return $this;
    }

    public function paginate(int $page = 1, int $limit = 20, array $columns = ['*']): array
    {
        if ($columns === ['*']) {
            $columns = $this->fields;
        }
        $res = $this->query->paginate($limit, $columns, '', $page);
        if (!$res) {
            return [];
        }
        $res = $res->toArray();
        return [
            'page'      => $res['current_page'],
            'last_page' => $res['last_page'],
            'limit'     => $res['per_page'],
            'total'     => $res['total'],
            'lists'     => $res['data'],
        ];
    }
}