<?php

declare(strict_types=1);

namespace Zct\Utils\Visitor;

use Hyperf\Database\Commands\Ast\AbstractVisitor;
use PhpParser\Node\Const_;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\Namespace_;

final class FieldVisitor extends AbstractVisitor
{
    /**
     * @param array $nodes
     * @return void
     */
    public function afterTraverse(array $nodes): void
    {
        $columnsStr = '';
        foreach ($this->data->getColumns() as $column) {
            if ($name = $column['column_name'] ?? null) {
                $info = '';
                foreach ($column as $k => $v) {
                    if ($v === null) {
                        $v = 'NULL';
                    } else {
                        $v = "'" . $v . "'";
                    }
                    $info .= (($info ? ',' : '') . "'$k'=>$v");
                }
                $columnsStr .= (($columnsStr ? ',' : '') . "'$name'=>[$info]");
            }
        }
        foreach ($nodes as $namespace) {
            if (!$namespace instanceof Namespace_) {
                continue;
            }
            foreach ($namespace->stmts as $class) {
                if (!$class instanceof Class_) {
                    continue;
                }
                $expr         = new ConstFetch(new Name("[$columnsStr]"));
                $prop         = new Const_('FIELDS', $expr);
                $propertyNode = new ClassConst([$prop], Class_::MODIFIER_PUBLIC);
                $addField     = true;
                foreach ($class->stmts as $key => $node) {
                    if (isset($node->consts[0]->name)
                        && $node->consts[0]->name->toString() === 'FIELDS') {
                        $addField           = false;
                        $class->stmts[$key] = $propertyNode;
                        break;
                    }
                }
                if ($addField) {
                    $class->stmts[] = $propertyNode;
                }
            }
        }
    }
}