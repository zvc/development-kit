<?php

declare(strict_types=1);

namespace Zct\Utils;

use Throwable;
use Zct\Utils\Interfaces\RequestInterface;

/**
 * 请求数据
 * Class Request
 * @package Zct\Utils
 */
final class Request implements RequestInterface
{
    public int        $fromId       = 0;//来源id-一般为发送消息的用户id，系统默认赋值，不可更改
    public array      $fromInfo     = [];//来源信息-一般为发送消息的用户基本信息，系统默认赋值，不可更改
    public int        $timestamp    = 0;//消息时间戳
    public string|int $type         = '';//消息类型
    public string     $request_type = 'HTTP';//请求类型
    public string     $path         = '';//路径
    public array      $params       = [];//调用方传递的参数
    public array      $headers      = [];//请求头数据
    public array      $serverParams = [];//请求环境相关参数

    private static Request $instance;

    public static function getInstance(): Request
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        $instance            = clone self::$instance;
        $instance->timestamp = time();
        return $instance;
    }

    public function set(string $key, int|string|array $data): self
    {
        try {
            $this->{$key} = $data;
        } catch (Throwable) {
        }
        return $this;
    }

    public function get(string $key): int|string|array|null
    {
        return $this->{$key} ?? null;
    }

    public function from(int $id, array $info = []): self
    {
        $this->fromId   = $id;
        $this->fromInfo = $info;
        return $this;
    }

    public function setUser(array $user): self
    {
        $this->from($user['id'] ?? 0, $user);
        return $this;
    }

    public function pack(): array
    {
        $packData                 = get_object_vars($this);
        $packData['fromInfo']     = (object)$packData['fromInfo'];
        $packData['toInfo']       = (object)$packData['toInfo'];
        $packData['params']       = (object)$packData['params'];
        $packData['headers']      = (object)$packData['headers'];
        $packData['serverParams'] = (object)$packData['serverParams'];
        return $packData;
    }

    public static function unpack(string|array $packData): self
    {
        $self = new self();
        if (is_string($packData)) {
            try {
                $packData = json_decode($packData, true, 512, JSON_THROW_ON_ERROR);
            } catch (Throwable) {
                $packData = [];
            }
        }
        foreach ($packData as $k => $v) {
            $self->{$k} = $v;
        }
        return $self;
    }

    public function getFromId(): int
    {
        return $this->fromId;
    }

    public function getFromInfo(): array
    {
        return $this->fromInfo;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRequestType(): string
    {
        return $this->request_type;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getHeader(string $key): int|string|array|null
    {
        return $this->headers[$key] ?? null;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getServerParams(): array
    {
        return $this->serverParams;
    }

    public function getServerParam(string $key): int|string|array|null
    {
        return $this->serverParams[$key] ?? null;
    }


}