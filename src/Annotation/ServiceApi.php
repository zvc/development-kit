<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: fsl
 * Date: 2020/7/7
 * Time: 14:53
 */

namespace Zct\Utils\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;
use Hyperf\Stringable\Str;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class ServiceApi extends AbstractAnnotation
{
    public function __construct(
        public string $name = '',
        public int $version = 1,
        public string $description = '',
        public string $path = '',
        public array $methods = ['GET', 'POST', 'DELETE', 'PUT']
    ) {
        foreach ($this->methods as $key => $method) {
            $methods[$key] = Str::upper(str_replace(' ', '', $method));
        }
    }
}