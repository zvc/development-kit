<?php

declare(strict_types=1);

namespace Zct\Utils\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class ServiceEvent extends AbstractAnnotation
{
    public function __construct(
        public string $value = '',
        public string|int|null $workerId = '',
    ) {
    }
}