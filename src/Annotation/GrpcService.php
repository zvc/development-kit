<?php

declare(strict_types=1);

namespace Zct\Utils\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;
use Zct\Utils\Constants\Grpc;

#[Attribute(Attribute::TARGET_CLASS)]
class GrpcService extends AbstractAnnotation
{
    public function __construct(
        public string $value = '',
        public string $name = '',
        public string $package = '',
        public string $server = Grpc::SERVER_NAME,
        public string $publishTo = 'consul',
    ) {
    }
}