<?php

declare(strict_types=1);

namespace Zct\Utils;

use Zct\Utils\Aspect\ConsulDriverAspect;
use Zct\Utils\Aspect\DbWriteAspect;
use Zct\Utils\Aspect\DispatcherFactoryAspec;
use Zct\Utils\Aspect\EventDispatchAspect;
use Zct\Utils\Aspect\EventListenerAspect;
use Zct\Utils\Aspect\ExceptionHandlerDispatcherAspect;
use Zct\Utils\Aspect\JsonRpcHttpServerAspect;
use Zct\Utils\Constants\Grpc;
use Zct\Utils\Constants\JsonRpcRequestParamMode;
use Zct\Utils\Exception\Handler\RuntimeExceptionHandler;
use Zct\Utils\Listener\BeforeServerListener;
use Zct\Utils\Listener\BeforeWorkerListener;
use Zct\Utils\Listener\DbRetrievedListener;
use Zct\Utils\Listener\DbSavingListener;
use Zct\Utils\Listener\RegisterRouteListener;
use Zct\Utils\Rpc\ServiceClient;
use Zct\Utils\Validation\ValidatorFactoryResolvedListener;
use Zct\Utils\Visitor\FieldVisitor;

class ConfigProvider
{
    private function readConfig(string $configPath): array
    {
        $config = [];
        if (file_exists($configPath) && is_readable($configPath)) {
            $config = require $configPath;
        }
        return is_array($config) ? $config : [];
    }


    public function __invoke(): array
    {
        $aspects      = [];
        $listeners    = [];
        $dependencies = [];
        if (class_exists('Hyperf\Event\ConfigProvider')) {
            $aspects   = [
                ConsulDriverAspect::class,
                ExceptionHandlerDispatcherAspect::class,
                JsonRpcHttpServerAspect::class,
                DbWriteAspect::class,
                EventDispatchAspect::class,
                EventListenerAspect::class,
                DispatcherFactoryAspec::class,
            ];
            $listeners = [
                BeforeServerListener::class,
                BeforeWorkerListener::class,
                DbSavingListener::class,
                DbRetrievedListener::class,
                ValidatorFactoryResolvedListener::class,
                RegisterRouteListener::class,
            ];
        }
        if (defined('BASE_PATH')) {
            $configPath = BASE_PATH . '/config/autoload/services.php';
            $config     = $this->readConfig($configPath);
            if (($config['request_param_mode'] ?? JsonRpcRequestParamMode::INDEXED) === JsonRpcRequestParamMode::ASSOCIATIVE &&
                class_exists('Hyperf\RpcClient\ConfigProvider')) {
                $dependencies = [
                    \Hyperf\RpcClient\ServiceClient::class => ServiceClient::class,
                ];
            }
        }
        return [
            'annotations'  => [
                'scan' => [
                    'paths' => [
                        __DIR__ . '/Annotation',
                    ],
                ],
            ],
            'exceptions'   => [
                'handler' => [
                    'jsonrpc-http'    => [
                        RuntimeExceptionHandler::class,
                    ],
                    'jsonrpc'         => [
                        RuntimeExceptionHandler::class,
                    ],
                    'http'            => [
                        RuntimeExceptionHandler::class,
                    ],
                    Grpc::SERVER_NAME => [
                        RuntimeExceptionHandler::class,
                    ],
                ],
            ],
            'dependencies' => $dependencies,
            'listeners'    => $listeners,
            'aspects'      => $aspects,
            'databases'    => [
                'default' => [
                    'commands' => [
                        'gen:model' => [
                            'visitors' => [
                                FieldVisitor::class
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}