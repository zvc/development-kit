<?php

declare(strict_types=1);

namespace Zct\Utils;

use Hyperf\Context\ApplicationContext;
use Hyperf\Contract\ConfigInterface;
use Redis;
use Throwable;

class UtilRedis
{
    private static $redis;

    public static function set(): void
    {
        $config      = ApplicationContext::getContainer()->get(ConfigInterface::class);
        $redisConfig = $config->get('redis')['default'] ?? null;
        if (!$redisConfig) {
            return;
        }
        $config->set('redis.ZctUtils', [
            'host'    => $redisConfig['host'] ?? '',
            'auth'    => $redisConfig['auth'] ?? '',
            'port'    => $redisConfig['port'] ?? '',
            'db'      => $redisConfig['db'] ?? '',
            'options' => [
                Redis::OPT_READ_TIMEOUT => -1,
            ],
        ]);
        $class = 'Hyperf\Redis\RedisFactory';
        if (ApplicationContext::getContainer()->has($class)) {
            self::$redis = ApplicationContext::getContainer()->get($class)->get('ZctUtils');
        } else {
            self::$redis = new Redis();
            self::$redis->connect($redisConfig['host'], $redisConfig['port']);
        }
    }

    public static function get()
    {
        if (!self::$redis) {
            try {
                self::set();
            } catch (Throwable $throwable) {
                Log::error('UtilRedis get:' . $throwable->getMessage());
            }
        }
        return self::$redis;
    }
}