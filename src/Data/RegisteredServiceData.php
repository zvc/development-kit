<?php

declare(strict_types=1);

namespace Zct\Utils\Data;

use JsonException;
use Throwable;

//已注册的服务数据
class RegisteredServiceData
{
    private array $serviceData = [];

    private static self $instance;

    public const  REGISTERED_SERVICE_FILE_PATH = BASE_PATH . '/runtime/registerService.cache';

    public static function getInstance(): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function readFileContent(): void
    {
        try {
            $this->serviceData = [];
            if (!file_exists(self::REGISTERED_SERVICE_FILE_PATH)) {
                return;
            }
            $contents = file_get_contents(self::REGISTERED_SERVICE_FILE_PATH);
            $contents = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
            if (is_array($contents)) {
                $this->serviceData = $contents;
            }
        } catch (Throwable $throwable) {
            logger()->debug($throwable->getMessage());
            logger()->debug(self::REGISTERED_SERVICE_FILE_PATH);
        }
    }

    public function add(string $protocol, string $name, string $host, int $port): void
    {
        $this->serviceData[$name] = $protocol;
        $fp                       = fopen(self::REGISTERED_SERVICE_FILE_PATH, 'wb');
        try {
            fwrite($fp, json_encode($this->serviceData, JSON_THROW_ON_ERROR));
        } catch (JsonException) {
        }
        fclose($fp);
    }

    public function check(string $protocol, string $name): bool
    {
        if (isset($this->serviceData[$name]) && $this->serviceData[$name] === $protocol) {
            return true;
        }
        $this->readFileContent();
        return isset($this->serviceData[$name]) && $this->serviceData[$name] === $protocol;
    }
}