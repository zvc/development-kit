<?php

declare(strict_types=1);

namespace Zct\Utils\Data;


class ModelJsonFieldData
{
    public static array $fields = [];

    public static function setFields(string $key, array $fields): void
    {
        self::$fields[$key] = [];
        foreach ($fields as $file) {
            if ($file['column_type'] === 'json') {
                self::$fields[$key][] = $file['column_name'];
            }
        }
    }

    public static function getFields(object $model): array
    {
        if (!$model::FIELDS) {
            return [];
        }
        $class = get_class($model);
        if (!isset(self::$fields[$class])) {
            self::setFields($class, $model::FIELDS);
        }
        return self::$fields[$class];
    }
}