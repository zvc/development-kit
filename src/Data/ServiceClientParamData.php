<?php

declare(strict_types=1);

namespace Zct\Utils\Data;

use Hyperf\Context\ApplicationContext;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionMethod;

class ServiceClientParamData
{
    private static array $serviceParams = [];

    private ContainerInterface $container;

    private static ?self $instance = null;

    public static function getMethodReflectParams(ReflectionMethod $methodRef): array
    {
        $params = [];
        foreach ($methodRef->getParameters() as $parameter) {
            $params[$parameter->getPosition()] = $parameter->getName();
        }
        return $params;
    }

    public static function setFormalParamMap(string $class): array
    {
        $container = ApplicationContext::getContainer()->get($class);
        $classRef  = new ReflectionClass(get_class($container));
        $methodMap = [];
        foreach ($classRef->getMethods() as $method) {
            $methodName = $method->getName();
            if ($methodName === '__construct') {
                continue;
            }
            $methodMap[$methodName] = self::getMethodReflectParams($method);
        }
        self::$serviceParams[$class] = $methodMap;
        return $methodMap;
    }

    public static function getFormalParams(string $class, string $method): ?array
    {
        $methodMap = self::$serviceParams[$class] ?? self::setFormalParamMap($class);
        return $methodMap[$method] ?? null;
    }
}