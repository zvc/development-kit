<?php

declare(strict_types=1);

namespace Zct\Utils\Data;

use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Event\ListenerData;
use Zct\Utils\Annotation\ServiceEvent;
use Zct\Utils\Listener\ServiceEventListener;

class ServiceEventListenerData
{
    /**
     * @var ListenerData[]
     */
    public static array $listenerDatas = [];
    public static array $listeners     = [];

    public static function listenerStart(int $workerId): void
    {
        if (count(self::$listenerDatas)) {
            foreach (self::$listenerDatas as $listenerData) {
                if (self::isEventListener($listenerData->event, $workerId)) {
                    self::$listeners[] = new ServiceEventListener($listenerData);
                }
            }
        }
    }

    private static function isEventListener(string $event, int $workerId): bool
    {
        $annotation = AnnotationCollector::getClassAnnotation($event, ServiceEvent::class);
        if ($annotation->workerId === null) {
            return true;
        }
        switch (gettype($annotation->workerId)) {
            case 'integer':
                if ($annotation->workerId !== $workerId) {
                    return false;
                }
                break;
            case 'string':
                $workerIds = explode(',', $annotation->workerId);
                if (!in_array($workerId, $workerIds, false)) {
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }
}