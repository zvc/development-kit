<?php

declare(strict_types=1);

namespace Zct\Utils\Data;

class ServiceEventDispatchData
{
    public static array $events = [];
}