<?php

declare(strict_types=1);

namespace Zct\Utils\Grpc\Container;

class Method
{
    public string $name    = ''; //方法名
    public string $path    = ''; //路由路径
    public string $handler = ''; //处理方法
}