<?php

declare(strict_types=1);

namespace Zct\Utils\Grpc\Container;

class Container
{
    /**
     * @var Server[] $servers
     */
    private static array $servers = [];

    /**
     * @param Server $server
     * @return void
     */
    public static function add(Server $server): void
    {
        if ($server->name === '') {
            return;
        }
        if (self::has($server->name)) {
            $serverOriginal = self::get($server->name);
            if ($serverOriginal) {
                foreach ($serverOriginal->methods as $key => $method) {
                    if (!isset($server->methods[$key])) {
                        $server->methods[$key] = $method;
                    }
                }
            }
        }
        self::$servers[$server->name] = $server;
    }

    /**
     * @param string $name
     * @return bool
     */
    public static function has(string $name): bool
    {
        return isset(self::$servers[$name]);
    }

    /**
     * @param string $name
     * @return Server|null
     */
    public static function get(string $name): ?Server
    {
        return self::$servers[$name] ?? null;
    }

    /**
     * @return Server[]
     */
    public static function getAll(): array
    {
        return self::$servers;
    }
}