<?php

declare(strict_types=1);

namespace Zct\Utils\Grpc\Container;

class Server
{
    public string $name    = ''; //服务名
    public string $package = '';//包名
    
    /**
     * @var Method[] $methods
     */
    public array  $methods   = []; //方法
    public string $publishTo = '';
}