<?php

declare(strict_types=1);

namespace Zct\Utils\Grpc\Server;

use Zct\Utils\Grpc\Health\V1\HealthCheckRequest;
use Zct\Utils\Grpc\Health\V1\HealthCheckResponse;

class Health
{
    public function Check(HealthCheckRequest $params): HealthCheckResponse
    {
        $messages = new HealthCheckResponse();
        $messages->setStatus(1);
        return $messages;
    }
}