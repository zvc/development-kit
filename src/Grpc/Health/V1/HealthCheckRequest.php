<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: HealthCheck.proto

namespace Zct\Utils\Grpc\Health\V1;

use Exception;
use Google\Protobuf\Internal\GPBUtil;
use Google\Protobuf\Internal\Message;
use Zct\Utils\Grpc\GPBMetadata\HealthCheck;

/**
 * Generated from protobuf message <code>zct.utils.grpc.health.v1.HealthCheckRequest</code>
 */
class HealthCheckRequest extends Message
{
    /**
     * Generated from protobuf field <code>string service = 1;</code>
     */
    protected string $service = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $service
     * }
     */
    public function __construct($data = NULL) {
        HealthCheck::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string service = 1;</code>
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * Generated from protobuf field <code>string service = 1;</code>
     * @param string $var
     * @return $this
     * @throws Exception
     */
    public function setService(string $var): self
    {
        GPBUtil::checkString($var, True);
        $this->service = $var;

        return $this;
    }

}

