<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: HealthCheck.proto

namespace Zct\Utils\Grpc\GPBMetadata;

use Google\Protobuf\Internal\DescriptorPool;

class HealthCheck
{
    public static bool $is_initialized = false;

    public static function initOnce(): void
    {
        $pool = DescriptorPool::getGeneratedPool();

        if (static::$is_initialized) {
            return;
        }
        $pool->internalAddGeneratedFile(
            '
�
HealthCheck.protozct.utils.grpc.health.v1"%
HealthCheckRequest
service (	"�
HealthCheckResponseK
status (2;.zct.utils.grpc.health.v1.HealthCheckResponse.ServingStatus":

ServingStatus
UNKNOWN 
SERVING
NOT_SERVING2n
Healthd
Check,.zct.utils.grpc.health.v1.HealthCheckRequest-.zct.utils.grpc.health.v1.HealthCheckResponseB�Zct/Utils/Grpc/GPBMetadatabproto3'
            ,
            true
        );

        static::$is_initialized = true;
    }
}

