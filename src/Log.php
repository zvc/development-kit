<?php

declare(strict_types=1);

namespace Zct\Utils;

use Hyperf\Context\ApplicationContext;
use Hyperf\Contract\StdoutLoggerInterface;
use JsonException;
use Throwable;
use Zct\Utils\Amqp\Amqp;
use Zct\Utils\Amqp\Producer\PrintProducer;

use function Hyperf\Coroutine\go;

/**
 * 日志类
 * @method static emergency($message, array $context = [])
 * @method static alert($message, array $context = [])
 * @method static critical($message, array $context = [])
 * @method static error($message, array $context = [])
 * @method static warning($message, array $context = [])
 * @method static notice($message, array $context = [])
 * @method static info($message, array $context = [])
 * @method static debug($message, array $context = [])
 * @method static log($level, $message, array $context = [])
 */
final class Log
{
    private static ?self $instance = null;

    private StdoutLoggerInterface $logger;

    public function __construct()
    {
        $this->logger = ApplicationContext::getContainer()->get(StdoutLoggerInterface::class);
    }

    public function call(string $name, array $arguments): void
    {
        if (Amqp::isEnabled()) {
            $this->amqpPush($name, $arguments);
        }
        $messageKey = ($name === 'log') ? 1 : 0;
        if (is_array($arguments[$messageKey]) || is_object($arguments[$messageKey])) {
            try {
                $arguments[$messageKey] = json_encode(
                    $arguments[$messageKey],
                    JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE
                );
            } catch (JsonException) {
                $arguments[$messageKey] = serialize($arguments[$messageKey]);
            }
        }
        if ($this->logger) {
            $this->logger->$name(...$arguments);
        } else {
            echo ($arguments[$messageKey] ?? '') . PHP_EOL;
        }
    }

    private function amqpPush(string $name, array $arguments): void
    {
        $pack = internalRequest()->pack();
        go(function () use ($name, $arguments, $pack) {
            try {
                $args = [];
                if ($name === 'log') {
                    $args['level']   = $arguments[0] ?? null;
                    $args['message'] = $arguments[1] ?? null;
                    $args['context'] = $arguments[2] ?? null;
                } else {
                    $args['message'] = $arguments[0] ?? null;
                    $args['context'] = $arguments[1] ?? null;
                }
                Amqp::push(PrintProducer::class, [
                    'trigger_time' => date('Y-m-d H:i:s'),
                    'type'         => $name,
                    'arguments'    => $args,
                    'requestInfo'  => $pack
                ]);
            } catch (Throwable $throwable) {
                logger()->debug($throwable->getMessage());
            }
        });
    }

    public static function instance(): self
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function __callStatic($name, $arguments): void
    {
        try {
            self::instance()->call($name, $arguments);
        } catch (Throwable $throwable) {
            logger()->debug($throwable->getMessage());
        }
    }
}